#!/usr/bin/python3

#######################################################################
# Description:                                                        # 
# This algorithm depends on sequencing segments from the static file, # 
# this would give a help in conflation different datasets with each   # 
# other. This algorithm consists of several steps:                    #
# 1. Find highway terminal points                                     #
# 2. Find the next segments distance matrix                           #                                                     
# 3. Repeat step 2 for the next segment until reaching the final      # 
# segment                                                             #
#######################################################################

import pandas as pd
import numpy as np
import os.path
import os
from PIL import Image
import sys
from os.path import basename
import tensorflow as tf
from tensorflow.python.framework import ops
from tensorflow.python.framework import dtypes
import cv2
from matplotlib import pyplot as plt

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
IMAGE_HEIGHT = 1280
IMAGE_WIDTH = 720
NUM_CHANNELS = 3

def read_labeled_image_list(image_list_file):
    """Reads a .txt file containing pathes and labeles
    Args:
       image_list_file: a .txt file with one /path/to/image per line
       label: optionally, if set label will be pasted after each line
    Returns:
       List with all filenames in file image_list_file
    """          
    dataset = pd.DataFrame.from_csv(BASE_DIR + '/dataset/' + image_list_file, index_col = None, encoding='utf-8')            
    filenames = []
    labels = dataset.Class.tolist()
    for i in range(dataset.shape[0]):      
        video_name = dataset['Video File Name'][i]
        video_name = os.path.splitext(video_name)[0]
        file_path = BASE_DIR + '/dataset/Video Data-03-08-2015/Data and Pics for ' + video_name + '/WIM16_Images for ' + video_name + '/'
        filename = file_path + dataset['Link 1'][i].split('\\')[1]
        filenames.append(filename)        
    return filenames, labels

def read_images_from_disk(input_queue):
    """Consumes a single filename and label as a ' '-delimited string.
    Args:
      filename_and_label_tensor: A scalar string tensor.
    Returns:
      Two tensors: the decoded image, and the string label.
    """    
    #image = open(filenames[0], 'r+') 
    #print image
    #file_contents = tf.read_file(filenames[0])
    #example = tf.image.decode_png(file_contents)
    #return example, labels


    label = input_queue[1] 
    file_contents = tf.read_file(input_queue[0])    
    example = tf.image.decode_png(file_contents, channels=NUM_CHANNELS)
    example.set_shape([IMAGE_HEIGHT, IMAGE_WIDTH, NUM_CHANNELS])
    return example, label

# Reads pfathes of images together with their labels
dataset_path = 'Video Data-03-08-2015/dataset.csv'
image_list, label_list = read_labeled_image_list(dataset_path)


# Here is how to read image using opencv (Only for test)
# img = cv2.imread(image_list[1],3)
# print img
# plt.imshow(img, interpolation = 'bicubic')
# plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
# plt.show()


# # step 2
# filename_queue = tf.train.string_input_producer(image_list)

# # step 3: read, decode and resize images
# reader = tf.WholeFileReader()
# filename, content = reader.read(filename_queue)
# image = tf.image.decode_jpeg(content, channels=3)
# image = tf.cast(image, tf.float32)
# resized_image = tf.image.resize_images(image, [224, 224])

# # step 4: Batching
# image_batch = tf.train.batch([resized_image], batch_size=8)



# print type(tf.Session().run(image))


# images = tf.convert_to_tensor(image_list)
# labels = tf.convert_to_tensor(label_list)

# # Makes an input queue
# input_queue = tf.train.slice_input_producer([images, labels], shuffle=True)

# image, label = read_images_from_disk(input_queue)

# # Optional Preprocessing or Data Augmentation
# # tf.image implements most of the standard image augmentation
# #image = preprocess_image(image)
# #label = preprocess_label(label)

# # Optional Image and Label Batching
# image_batch, label_batch = tf.train.batch([image, label], batch_size=100)
# with tf.Session() as sess:
#   tf.global_variables_initializer().run()
#   image_tensor = sess.run()
#   print(image_batch.eval())

